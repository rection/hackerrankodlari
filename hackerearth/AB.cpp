/*
 * =====================================================================================
 *
 *       Filename:  AB.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2020-06-06 09:52:57 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Safa Bayar (rection), safa@safabayar.tech
 *   Organization:  
 *
 * =====================================================================================
 */

/*
// Sample code to perform I/O:

#include <iostream>

using namespace std;

int main() {
	int num;
	cin >> num;										// Reading input from STDIN
	cout << "Input number is " << num << endl;		// Writing output to STDOUT
}

// Warning: Printing unwanted or ill-formatted data to output will cause the test cases to fail
*/

// Write your code here


// #include <iostream>
// #include <string> 

#include <bits/stdc++.h>

using namespace std;


int main(){

for ( int i = 0; i < 13; i++ ){
    string a,b,str;
    cin >> a >> b;

    if(a.length() > b.length()){
        swap(a,b);
    }

    reverse(a.begin(), a.end());
    reverse(b.begin(), b.end());

    int carry = 0;
    for(int i = 0; i < a.size();i++){
        int sum = ((a[i]-'0') + (b[i] - '0') + carry);
        str.push_back(sum%10 + '0');
        carry = sum/10;
    }

    for(int l = a.size(); l < b.size(); l++){
        int sum = ((b[l] - '0') + carry);
        str.push_back(carry + '0');
        carry = sum/10;
    }

    // if(carry){
    //     str.push_back(carry+'0');
    // }

    reverse(str.begin(),str.end());

    cout << str << endl;
}

    return 0;




//    unsigned long long int a[2], sum[13] = {0,0,0,0,0,0,0,0,0,0,0,0,0};
//    for(int k = 0; k < 13; k++ ){
//        for(int i = 0; i < 2; i++){
//            cin >> a[i];
//        }
//
//        for(int i = 0; i < 2; i++){
//            sum[k] += a[i];
//        }
//    }
//    for(int i = 0; i < 13; i++ ){
//        if(sum[i] != sum[i-1])
//            cout << sum[i] << endl;
//    }
//

}
