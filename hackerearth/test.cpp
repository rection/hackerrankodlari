/*
 * =====================================================================================
 *
 *       Filename:  test.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2020-06-07 12:51:07 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Safa Bayar (rection), safa@safabayar.tech
 *   Organization:  
 *
 * =====================================================================================
 */

    #include<bits/stdc++.h>
    using namespace std;
    int main()
    {
    	int t=13;
    	for(int i=0;i<t;i++)
    	{
    		string a,b;
    		cin>>a>>b;
    		long long int signed p,q,j;
    		if(a.size()>b.size())
    		{
    			swap(a,b);
    		}		
    		p=a.size();
    		q=b.size();
    		reverse(a.begin(),a.end());
    		reverse(b.begin(),b.end());
    		string str="";
    		int carry=0,sum;
    		for(j=0;j<p;j++)
    		{
    			sum=(a[j]-'0')+(b[j]-'0')+carry;
    			str.push_back(sum%10+'0');
    			carry=sum/10;
    		}
    		for(j=p;j<q;j++)
    		{
    			sum=(b[j]-'0')+carry;
    		    str.push_back(sum%10+'0');
    			carry=sum/10;
    		}
    		
    		if(carry!=0)
    		{
    		str.push_back(carry+'0');
    	    }
    		reverse(str.begin(),str.end());
    		cout<<str<<endl;
    	}
    	return 0;
    }
